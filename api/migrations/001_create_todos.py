steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE todos (
            id SERIAL PRIMARY KEY NOT NULL,
            action VARCHAR(100) NOT NULL,
            description TEXT,
            value INTEGER NOT NULL DEFAULT 1
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE todos;
        """
    ],
]
