"""
Pydantic Models for Todos
"""
from pydantic import BaseModel


class TodoRequest(BaseModel):
    """
    Model Representing a new Todo
    """

    action: str
    description: str
    value: int | str


class TodoResponse(BaseModel):
    """
    Model representing a Todo
    """

    id: int
    action: str
    description: str
    value: int
    user_id: int
