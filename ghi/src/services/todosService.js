// @ts-check
import { baseUrl } from './authService'

/**
 * @typedef {import('../types').Todo} Todo
 * @typedef {import('../types').TodoFormData} TodoFormData
 */

/** Create a new Todo
 * @param {TodoFormData} todoData
 * @return {Promise<Todo>}
 */
export async function createTodo(todoData) {
    if (!todoData.action || !todoData.description || !todoData.value) {
        throw new Error('Action, Description and Value are Required.')
    }

    // validate the value as a number
    if (isNaN(todoData.value)) {
        throw new Error('Value must be a number')
    }

    try {
        const res = await fetch(`${baseUrl}/api/todos`, {
            method: 'post',
            credentials: 'include',
            body: JSON.stringify(todoData),
            headers: {
                'Content-Type': 'application/json',
            },
        })
        if (!res.ok) {
            throw new Error('Could not create a new todo')
        }
        const data = await res.json()
        return data
    } catch (e) {
        if (e instanceof Error) {
            throw new Error('Could not create a new todo')
        }
        throw new Error('Unknown Error')
    }
}

/** Get all the todos from the API
 * @return {Promise<Todo[]>}
 */
export async function getTodos() {
    try {
        const res = await fetch(`${baseUrl}/api/todos`, {
            method: 'get',
            credentials: 'include',
        })
        if (!res.ok) {
            throw new Error('Could not fetch list of todos')
        }
        const data = await res.json()
        return data
    } catch (e) {
        if (e instanceof Error) {
            console.error(e)
            throw new Error('Could not fetch list of todos')
        }
        throw new Error('Unknown Error')
    }
}
