export {}

/** @typedef {Todo[]} Todos */

/**
 * @typedef {Object} AuthContextType
 * @property {Error} [error]
 * @property {(error:Error)=>void} setError
 * @property {UserDataResponse} [user]
 * @property {(user?:UserDataResponse)=>void} setUser
 * @property {boolean} isLoading
 * @property {(state:boolean)=>void} setIsLoading
 * @property {boolean} isLoggedIn
 */
/**
 * @typedef {Object} SignInRequest
 * @property {string} username
 * @property {string} password
 */
/**
 * @typedef {Object} SignUpRequest
 * @property {string} username
 * @property {string} password
 */
/**
 * @typedef {Object} UserDataResponse
 * @property {number} id
 * @property {string} username
 * @property {string} email
 */
/**
 * @typedef {Object} TodoFormData
 * @property {string} action
 * @property {string} description
 * @property {number} value
 */
/**
 * @typedef {Object} CreateTodoResponse
 * @property {Error} [error]
 * @property {Todo} [newTodo]
 */
/**
 * @typedef {Object} GetTodosResponse
 * @property {Todo[]} todos
 * @property {Error} [error]
 */
/**
 * @typedef {Object} Todo
 * @property {number} id
 * @property {string} action
 * @property {string} description
 * @property {number} value
 * @property {number} user_id
 */
